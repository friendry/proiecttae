package CoffeShopApplication.DAO;


import CoffeShopApplication.Entities.Customer;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepository extends JpaRepository<Customer,Long>{//CrudRepository<Customer, Long>{

}

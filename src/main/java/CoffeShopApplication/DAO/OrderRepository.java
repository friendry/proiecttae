package CoffeShopApplication.DAO;

import CoffeShopApplication.Entities.CustomerOrder;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderRepository extends JpaRepository<CustomerOrder, Long>{//CrudRepository<CustomerOrder,Long>{

}

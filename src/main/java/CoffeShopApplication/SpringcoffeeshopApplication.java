package CoffeShopApplication;


import CoffeShopApplication.DAO.*;
import CoffeShopApplication.Entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackageClasses=ProductsController.class)
//@ComponentScan(basePackageClasses=OrdersController.class)
public class SpringcoffeeshopApplication implements CommandLineRunner {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrderRepository orderRepository; 



    public static void main(String[] args) {
		SpringApplication.run(SpringcoffeeshopApplication.class, args);
	}


    @Override
    public void run(String... strings) throws Exception {

        Product latte = new Product();
        latte.setProductName("Caffe Latte");
        latte.setProductPrice(2.55);

        Product expresso = new Product();
        expresso.setProductName("Expresso");
        expresso.setProductPrice(5.25);

        productRepository.save(latte);
        productRepository.save(expresso);


    } 


}

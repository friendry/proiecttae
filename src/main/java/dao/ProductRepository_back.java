package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import CoffeShopApplication.Entities.Product;

public interface ProductRepository_back
        extends JpaRepository<Product, Long>{
    
}
